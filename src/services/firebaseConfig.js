import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { initializeAuth, getReactNativePersistence } from 'firebase/auth';
import ReactNativeAsyncStorage from '@react-native-async-storage/async-storage';

const firebaseConfig = {
  apiKey: "AIzaSyD69pTiBkkFvN-omyJ4VzaJlsxE0E071qw",
  authDomain: "teste-firebase-dudu.firebaseapp.com",
  projectId: "teste-firebase-dudu",
  storageBucket: "teste-firebase-dudu.appspot.com",
  messagingSenderId: "379673302413",
  appId: "1:379673302413:web:4534175d7ace6e5e82d9c4",
  measurementId: "G-LCS3ZTTNVE"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const auth = initializeAuth(app, {
  persistence: getReactNativePersistence(ReactNativeAsyncStorage)
});

export const db = getFirestore(app);